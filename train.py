import sys

import pandas as pd
import nltk
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import train_test_split
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
import mlflow
from unidecode import unidecode

def tentar_registrar_experimento(accuracy, dataset, model):
    with mlflow.start_run():
        mlflow.log_metric("acuracia", accuracy)
        mlflow.log_artifact(dataset)
        mlflow.sklearn.log_model(model, "modelo")

if __name__ == "__main__":
    nltk.download('stopwords')
    p_test_size = float(sys.argv[1]) if len(sys.argv) > 1 else 0.2
    p_include_names = sys.argv[2] if len(sys.argv) > 2 else 'sim'

    print("Treinando classificador de modelos...")
    print(f"Tamanho de testes={p_test_size}")
    print(f"Incluir nomes={p_include_names}")

    dataset = 'produtos.csv'

    products_data = pd.read_csv(dataset, delimiter=';', encoding='utf-8')

    if p_include_names == 'sim':
        products_data['informacao'] = products_data['nome'] + ' ' + products_data['descricao']
    else:
        products_data['informacao'] = products_data['descricao']

    products_data.dropna(subset=['informacao', 'categoria'], inplace=True)
    products_data.drop(columns=['nome', 'descricao'], inplace=True)

    stop_words=set(stopwords.words("portuguese"))
    # products_data['sem_stopwords'] = products_data['informacao'].str.lower().apply(lambda x: ' '.join([word for word in x.split() if word not in (stop_words)]))
    products_data['sem_stopwords'] = products_data['informacao'].str.lower().apply(lambda x: ' '.join([unidecode(word) for word in x.split() if word not in (stop_words)]))


    tokenizer = nltk.RegexpTokenizer(r"\w+")

    products_data['tokens'] = products_data['sem_stopwords'].apply(tokenizer.tokenize)

    products_data.drop(columns=['sem_stopwords','informacao'],inplace=True)

    products_data["strings"]= products_data["tokens"].str.join(" ")
    
    X_train,X_test,y_train,y_test = train_test_split(
        products_data["strings"], 
        products_data["categoria"], 
        test_size = p_test_size, 
        random_state = 10
    )

    pipe = Pipeline([('vetorizador', CountVectorizer()), ("classificador", MultinomialNB())])

    pipe.fit(X_train, y_train)

    y_prediction = pipe.predict(X_test)
    accuracy = accuracy_score(y_prediction, y_test)

    print(f"Acurácia={accuracy}")
    
    tentar_registrar_experimento(accuracy, dataset, pipe)